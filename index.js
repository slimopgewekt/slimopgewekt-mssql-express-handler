const sql = require("mssql");
const SINGLEROW = 'SINGLEROW';
const MULTIPLEROW = 'MULTIPLEROW';
const INSERTID = 'INSERTID';
const NORESPONSE = 'NORESPONSE';

connectionConfig = {
	user: process.env.DBUSER,
	password: process.env.DBPASS,
	server: process.env.DBSERV, 
	database: process.env.DBNAME,
	encrypt: true
};

function recordsetToArray (recordset){
	return Object.keys(recordset).map(key => {
		return recordset[key];
	})
}
function handleResult(result, resultType) {
	if(resultType === NORESPONSE)
		return null;
	if(resultType === INSERTID)
		return result && result.recordset.length > 0 ? result.recordset[0].id : null;
	if(resultType === SINGLEROW)
		return result.recordset.length >= 1 ? result.recordset[0] : recordsetToArray(result.recordset);
	else
		return recordsetToArray(result.recordset);
}
const addInsertId = (query) => {
	if(!query || query.length === 0)
		return query;
	query = query.trim();
	if(query[query.length - 1] != ';')
		query += ';';
	query += 'SELECT SCOPE_IDENTITY() as id;';
	return query;
}

function queryHandler (query, resultType = MULTIPLEROW){
	return async function(req, res, next) {
		try{
			const result = await functionQuery(query, resultType);
			res.send(result);
		}catch(err){
			console.log('queryError', err);
			res.send(err);
		}finally{
			sql.close();
			next();
		}
	}
}

function parameterizedQueryHandler (query, fromBody = true, resultType = NORESPONSE) {
	return async function(req, res, next) {
		try{
			const data = fromBody ? req.body : req.params;
			const params = [];
			Object.keys(data).forEach((key) => {
				params.push( {name: key, value: data[key] != 'null' ?  data[key] : null});	
			});
			const result = await queryWithParams(query, params, resultType);
			res.send(result);
		}catch(err){
			console.log('queryError', err);
			res.send(err);
		}finally{
			sql.close();
			next();
		}
	}
}

const functionQuery = async (query, resultType = SINGLEROW) => {
	if(resultType === INSERTID)
		query = addInsertId(query);
	const pool = await new sql.ConnectionPool(connectionConfig).connect();
	const result = await pool.request().query(query);
	sql.close();
	return handleResult(result, resultType);
}

const queryWithParams = async (query, params = [], resultType = SINGLEROW) => {
	if(resultType === INSERTID)
		query = addInsertId(query);
	const pool = await new sql.ConnectionPool(connectionConfig).connect();
	const request = pool.request();
	
	params.forEach(param => {
		if(param.type)
			request.input(param.name, param.type, param.value);
		else	
			request.input(param.name, param.value)
	})
	
	const result = await request.query(query);
	sql.close();
	return handleResult(result, resultType);
}

module.exports = {
  queryHandler,
  parameterizedQueryHandler,
  query : functionQuery,
  queryWithParams,
  SINGLEROW,
  MULTIPLEROW,
  NORESPONSE,
  INSERTID,
  types : {
	VarChar: sql.VarChar,
	Int: sql.Int,
	DateTime: sql.DateTime
  }
};